# colorecho

`$ colorecho '@_hallo' '@!welt'`

⇒ _hallo_ **welt**

`@X` is a single color code
`@{Xxy}` is a group of three codes

`colorecho '@rHallo @{_Y}Welt'` is Hallo with red foreground and a Welt with red foreground, yellow background and underline.

the color codes are:

* `~`: RESET
* `!`: BOLD
* `_`: UNDERLINE
* `%`: IMAGE_NEGATIVE
* `&`: IMAGE_POSITIVE
* `.`: NORMAL_INTENSITY
* `k`: FOREGROUND_BLACK
* `r`: FOREGROUND_RED
* `g`: FOREGROUND_GREEN
* `y`: FOREGROUND_YELLOW
* `b`: FOREGROUND_BLUE
* `m`: FOREGROUND_MAGENTA
* `c`: FOREGROUND_CYAN
* `w`: FOREGROUND_WHITE
* `d`: FOREGROUND_DEFAULT
* `K`: BACKGROUND_BLACK
* `R`: BACKGROUND_RED
* `G`: BACKGROUND_GREEN
* `Y`: BACKGROUND_YELLOW
* `B`: BACKGROUND_BLUE
* `M`: BACKGROUND_MAGENTA
* `C`: BACKGROUND_CYAN
* `W`: BACKGROUND_WHITE
* `D`: BACKGROUND_DEFAULT


