package main

import (
	"flag"
	"fmt"

	"bitbucket.org/leiji/ansi"
)



func main() {
	noNewLine := flag.Bool("n", false, "Omit new line at the end.")
	noLineReset := flag.Bool("r", false, "Do not reset the color at the end of the line.")
	noArgumentReset := flag.Bool("R", false, "Do not reset the color at the end of each argument.")
	
	flag.Parse()

	argReset := "@~"
	if *noArgumentReset {
		argReset = ""
	}
	
	for i, arg := range flag.Args() {
		s := fmt.Sprintf("%s%s", arg, argReset)
		ansi.Print(s)
		if i < flag.NArg() - 1 {
			ansi.Print(" ")
		}
	}
	if !*noLineReset {
		ansi.Print("@~")
	}

	if !*noNewLine {
		ansi.Print("\n")
	}



}
